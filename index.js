var reporter = require("cucumber-html-reporter");
var options = {
  theme: "bootstrap",
  jsonFile: "reports/reportsJSON/report.json",
  output: "reports/reportsHTML/cucumber_report.html",
  reportSuiteAsScenarios: true,
  scenarioTimestamp: true,
  launchReport: true,
  metadata: {
    Title: "TestCafe Report",
    Browser: "Chrome",
    Platform: "Windows 10",
  },
};
reporter.generate(options);