/* const { Given, When, Then } = require('cucumber');
const Button = require('../cores/Button');
const Assert = require("../cores/Assert")

const ClientPage = require('../pages/client/client-login-page');
const CreateAccount = require('../pages/client/client-create-account');
const ServicePage = require('../pages/client/question-bank-page');
const CheckLevel = require('../pages/client/check-level-page');
const PlanPage = require('../pages/client/select-plan-page');
const BillInfor = require('../pages/client/post-checkout');
const UpgradePage = require('../pages/client/upgrade-plan-page');
const ShippingInfor = require('../pages/client/shipping-infor');
const Delivery = require('../pages/client/emergency-contact-infor');
const CheckoutQuestions = require('../pages/client/post-checkout-questions');
const LoginPage = require("../pages/admin/admin-login-page");
const HomePage = require("../pages/admin/home-page");
const SchedulePrescriber = require("../pages/client/schedule-prescriber");
const ScheduleCounSelor = require("../pages/client/schedule-counselor");
const MessagePage = require("../pages/client/message-page");

const randomstring = require('randomstring');





Given('I navigate to login page from the landing page', async function () {
    await ClientPage.navigateToPage('https://app-fe-release.getcerebral.com/');
});
When('I click on “I dont have an account.“', async function () {
    await ClientPage.clickCreateAccount();
});
When('complete account information step', async function () {
    await CreateAccount.inputInforAccount(
        randomstring.generate({ length: 10 }) + "@gmail.com",
        randomstring.generate({ length: 10, charset: 'numeric' }),
        "Thang@_123");
});
When('complete personal information step', async function () {
    await CreateAccount.inputInforPersonal(
        randomstring.generate({ length: 5, charset: 'alphabetic' }),
        randomstring.generate({ length: 5, charset: 'alphabetic' }),
        "77089",
        "12121999"
    );
});
When('select {string} service line', async function (string) {
    await ServicePage.clickServices(string);
});
When('I select “Addiction” - {string} service lines', async function (string) {
    await ServicePage.clickServices("Substance Use");
    await Button.clickButton("Confirm", 1);
    await ServicePage.clickServices(string);
});
When('answer a list of questions regarding to Anxiety and Alcohol', async function () {
    await ServicePage.clickConfirm();
    await ServicePage.clickNext();
    await ServicePage.clickRandomOptionLoop();
    await Button.clickButton("Continue",1);
    await ServicePage.clickRandomOptionLoop();
    await ServicePage.clickRandomCheckBox();
    await ServicePage.clickConfirm();
});
Then('I see Preliminary Screen Indicators screen showing my Anxiety level and Alcohol level', async function () {
    await CheckLevel.checkMyLevelDisplayed();
});
When('I click Continue', async function () {
    await Button.clickButton("Continue", 1);
});
Then('I see Medication Care Counseling plan', async function () {
     await Assert.AssertEqual(PlanPage.firstPlan,"Medication &\nCare Counseling"); 
});
When('I click VIEW MORE PLANS', async function () {
    await PlanPage.clickViewMore();
});

Then('I see “Medication & Care Counseling“, “Medication & Therapy“, “Therapy“ plans', async function () {
    await PlanPage.allPlanDisplayed(
        "Medication &\nCare Counseling",
        "Medication &\nTherapy",
        "Therapy"
    );
});

When('I note down the first month price and discount price of “Medication & Care Counseling“ plan', async function () {
    await PlanPage.noteDiscountPrice();
    await PlanPage.notePriceLater();
});

When('select “Medication & Care Counseling“ plan', async function () {
    await PlanPage.clickButtonStartToday();
});

When('get navigated to Billing Information screen', async function () {
    await BillInfor.billingScreen("Billing Information");
});

Then('I see “Medication & Care Counseling“ which I chose', async function () {
    await BillInfor.checkMyChoose("Medication & Care Counseling");
});

When('the price for first month is like I noted and the price for month later is $20', async function () {
    await BillInfor.myDiscountPrice(PlanPage.myDiscountPrice);
    await BillInfor.myPriceLater(PlanPage.myPriceLater);
});

When('the promo code is “CARE30“', async function () {
    await BillInfor.myPromoteCode("CARE30");
});

When('I complete check out', async function () {
    await BillInfor.inputCreditCardInfor("hoangthang", "4242424242424242", "1222", "111", "77089");
});

Then('I see “Add Therapy to improve your care“ plan', async function () {
    await UpgradePage.upgradePlan("Add Therapy to improve \nyour care");
});

When('I click “Continue with current plan“ to skip', async function () {
    await UpgradePage.clickContinueCurrentPlan();
    await UpgradePage.clickRandomOption();
});

When('I complete shipping information', async function () {
    await ShippingInfor.inputShippingInfor("Washington,US", "Washington,US", "USA", "77089");
    await Button.clickButton("Next", 1);
});


When('I complete delivery method', async function () {
    await Delivery.inputEmergencyContact("Shiro", "Neeko", randomstring.generate({ length: 10, charset: 'numeric' }));
    await Button.clickButton("Next", 1);
});

When('complete post checkout questions', async function () {
    await CheckoutQuestions.clickRandomOption();
    await CheckoutQuestions.clickCheckBox();
    await Button.clickButton("Confirm",1);
    await Button.clickButton("No", 1);
    await CheckoutQuestions.selectWeightandHeight("60");
    await Button.clickButton("Confirm weight and height", 1);
    await CheckoutQuestions.clickRandomCheckBoxWithTextArea(CheckoutQuestions.textareaOption);
    await Button.clickButton("Confirm", 1);
    await CheckoutQuestions.clickRandomOptionLoop();
    await CheckoutQuestions.clickRandomCheckBoxWithTextArea(CheckoutQuestions.textareaSubstances);
    await Button.clickButton("Confirm",1);
    await Button.clickButton("No", 5);
    await CheckoutQuestions.inputTextArea();
    await Button.clickButton("Confirm your answer", 1);

});

When('complete “Identity Verification“', async function () {
    await LoginPage.navigateTo("https://release.getcerebral.com/associates/sign_in");
    await LoginPage.loginByAdmin("staging-admin@getcerebral.com", "Cerebral616");
    await HomePage.searchPatient(CreateAccount.getEmail());
    await HomePage.identityVerification();
});

When('select “I prefer a video visit“', async function () {
    await ClientPage.navigateTo("https://app-fe-release.getcerebral.com/");
    await ClientPage.loginMyAccount(
         CreateAccount.getEmail(),CreateAccount.getPassword() 
        
        );
    await Button.clickButtonWithTime("Continue", 1, 3000);
    await Button.clickButton("Next", 1);
    await Button.clickButton("I prefer a video visit", 1); 
    await Button.clickButton("Next", 1);
});

When('schedule appointment with prescriber', async function () {
    
    await SchedulePrescriber.clickDay();
    await SchedulePrescriber.clickTime();
    await Button.clickButton("Next", 2);
});

When('schedule appointment with counselor', async function () {

    await Button.clickButtonWithTime("Continue", 1, 4000);
    await Button.clickButton("Next", 1);
    await ScheduleCounSelor.clickDay();
    await ScheduleCounSelor.clickTime();
});

When('click “Continue to the client portal“', async function () {
    await Button.clickButtonWithTime("Continue to the client portal", 1, 3000);
});

Then('I get navigated to Message box chat', async function () {
    await MessagePage.verifyPageURL("https://app-fe-release.getcerebral.com/patient/dashboard/message");
});

When('see Welcome Message from care counselor', async function () {
    await MessagePage.verifyMessageDisplay();
});

















 */