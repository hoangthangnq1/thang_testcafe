const { Given, When, Then } = require('cucumber');
const Button = require('../cores/Button');
const Assert = require('../cores/Assert');

const ClientPage = require('../pages/client/client-login-page');
const CreateAccount = require('../pages/client/client-create-account');
const ServicePage = require('../pages/client/question-bank-page');
const CheckLevel = require('../pages/client/check-level-page');
const PlanPage = require('../pages/client/select-plan-page');
const BillInfor = require('../pages/client/post-checkout');
const UpgradePage = require('../pages/client/upgrade-plan-page');
const ShippingInfor = require('../pages/client/shipping-infor');
const Delivery = require('../pages/client/emergency-contact-infor');
const CheckoutQuestions = require('../pages/client/post-checkout-questions');
const LoginPage = require("../pages/admin/admin-login-page");
const HomePage = require("../pages/admin/home-page");
const SchedulePrescriber = require("../pages/client/schedule-prescriber");
const MessagePage = require("../pages/client/message-page");

const PharmacyPage = require("../pages/client/pharmacy");

const randomstring = require('randomstring');
const ScheduleCounselor = require('../pages/client/schedule-counselor');





Given('I navigate to login page from the landing page', async function () {
    await ClientPage.navigateToPage('https://app-fe-release.getcerebral.com/');
});
When('I click on “I dont have an account.”', async function () {
    await ClientPage.clickCreateAccount();
    
});
When('complete account information step', async function () {
    await CreateAccount.inputInforAccount(
        randomstring.generate({ length: 10 }) + "@gmail.com", 
        randomstring.generate({ length: 10, charset: 'numeric' }), 
        "Thang@_123"); 
});
When('completed personal information step', async function () {
    await CreateAccount.inputInforPersonal(
        randomstring.generate({ length: 5, charset: 'alphabetic' }),
        randomstring.generate({ length: 5, charset: 'alphabetic' }),
        "90225",
        "12121999"
    );
});
When('select {string} service line', async function (string) {
    await ServicePage.clickServices(string);
});

When('answer a list of questions regarding to ADHD', async function () {
    await ServicePage.clickConfirm();
    await ServicePage.clickNext();
    await ServicePage.clickRandomOptionLoop();
    await ServicePage.clickRandomCheckBox();
    await Button.clickButton("Confirm", 1);
});
Then('I see Preliminary Screen Indicators screen showing my “ADHD“ level', async function () {
    await CheckLevel.checkMyLevelDisplayed();
});
When('I click on “Continue”', async function () {
    await Button.clickButton("Continue", 1);
});
When('I click on “Start Today” on Medication & Care Counseling plan', async function () {
    await Button.clickButton("No", 1);
    await PlanPage.clickButtonStartToday();
});

Then('I get navigated to Billing Information screen', async function () {
    await BillInfor.billingScreen("Billing Information");
});

When('I see {string}', async function (string) {
    await BillInfor.checkMyChoose(string);
});

When('the price is for first month is ${string}', async function (string) {
    await BillInfor.myDiscountPrice(string);
});

When('the price for month later is ${string}', async function (string) {
    await BillInfor.myPriceLater(string);
});

When('I complete check out', async function () {
    await BillInfor.inputCreditCardInfor(
        "hoangthang",
        "4242424242424242",
        "1222",
        "111",
        "90225"
    );
});


When('click “Continue with current plan”', async function () {
    await UpgradePage.clickContinueCurrentPlan();
    await UpgradePage.clickRandomOption();
    await Button.clickButton("No", 1);
});

When('complete shipping information', async function () {
    await ShippingInfor.inputShippingInfor(
        "986 Pharmacy",
        "986 Pharmacy",
        "Los Angeles",
        "90225"
    );
    await PharmacyPage.clickButtonDelivery();
    await PharmacyPage.clickRandomPharmacy();
    await Button.clickButton("Continue", 1);
    await Button.clickButton("Next", 1);
    await Button.clickButton("No", 1);
});


When('complete prescription delivery method', async function () {
    await Delivery.inputEmergencyContact(
        "Shiro",
        "Neeko",
        randomstring.generate({ length: 10, charset: 'numeric' }));
    await Button.clickButton("Next", 1);
});

When('complete ADHD post checkout questions', async function () {
    await CheckoutQuestions.clickRandomOption();
    await Button.clickButton("No", 2);
    await CheckoutQuestions.clickCheckBox();
    await Button.clickButton("Confirm", 1);
    await Button.clickButton("No", 1);
    await CheckoutQuestions.selectWeightandHeight("60");
    await Button.clickButton("Confirm weight and height", 1);
    await CheckoutQuestions.clickRandomCheckBoxWithTextArea(
        CheckoutQuestions.textareaOption
    );
    await Button.clickButton("Confirm", 1);
    await CheckoutQuestions.clickRandomOptionLoop();
    await CheckoutQuestions.clickRandomCheckBoxWithTextArea(
        CheckoutQuestions.textareaSubstances
    );
    await Button.clickButton("Confirm", 1);
    await Button.clickButton("No", 5);
    await CheckoutQuestions.inputTextArea();
    await Button.clickButtonWithTime("Confirm your answer", 1, 3000);
});

When('complete “Identity Verification“', async function () {
    await LoginPage.navigateTo("https://release.getcerebral.com/associates/sign_in");
    await LoginPage.loginByAdmin("staging-admin@getcerebral.com", "Cerebral616");
    await HomePage.searchPatient(
        CreateAccount.getEmail()
    );
    await HomePage.identityVerification();

});

When('continue to the client portal', async function () {
    await ClientPage.navigateTo("https://app-fe-release.getcerebral.com/");
    await ClientPage.loginMyAccount(
        CreateAccount.getEmail(), CreateAccount.getPassword(),
    );

    await Button.clickButtonWithTime("Continue", 1, 3000); 
    await Button.clickButton("Next", 2);

    await SchedulePrescriber.clickDay();
    await SchedulePrescriber.clickTime();
    await Button.clickButton("Next",2);
    await Button.clickButtonWithTime("Continue", 1, 4000); 
    await Button.clickButton("Next", 1);

    await ScheduleCounselor.clickDay();
    await ScheduleCounselor.clickTime();
    await Button.clickButtonWithTime("Continue to the client portal", 1, 3000);
});

Then('I get navigated to Message box chat', async function () {
    await MessagePage.verifyPageURL("https://app-fe-release.getcerebral.com/patient/dashboard/message");
});

When('see greeting message from Cerebral Care team', async function () {
    await MessagePage.verifyMessageDisplay("I am your Cerebral Care Counselor");
});

















