const { Selector } = require('testcafe');


class LoginPage{

    constructor(){
        this.email = Selector("#associate_email");
        this.password = Selector("#associate_password");
        this.btnLogin = Selector ("input").withAttribute("value","Log in");
    }

    async openWindow(url){
        await testController.openWindow(url);
        await testController.maximizeWindow().wait(4000);
    }

    async navigateTo(url){
        await testController.setNativeDialogHandler(() => true);
        await testController.navigateTo(url).wait(4000);
    }

    async loginByAdmin(email, password){
        await testController.typeText(this.email,email);//"staging-admin@getcerebral.com"
        await testController.typeText(this.password,password); //"Cerebral616"
        await testController.click(this.btnLogin).wait(3000);
    }

 


   
}

module.exports =new LoginPage();