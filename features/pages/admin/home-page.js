const { Selector } = require('testcafe');


class HomePage{

    constructor(){
       this.searchBox = Selector("input").withAttribute("placeholder","Search Patients");
       this.btnSearch = Selector("button").withAttribute("class","btn btn-link btn-sm").nth(0);
       this.table = Selector("#patient-results");
       this.btnVerifyComplete = Selector("button").withText("Verification Complete");
       this.account = this.table.find("a").nth(0);
    }

    async searchPatient(string){
        await testController.typeText(this.searchBox,string);
        await testController.click(this.btnSearch).wait(2000);
    }

    async identityVerification(){
        await testController.click(this.account);
        await testController.setNativeDialogHandler(() => true).wait(1000); 
        await testController.scrollIntoView(this.btnVerifyComplete).wait(1000);  
        await testController.click(this.btnVerifyComplete).wait(1000);   
    }
   
}

module.exports =new HomePage();