const { Selector } = require('testcafe');
const { ClientFunction } = require('testcafe');
const getLocation = ClientFunction(() => document.location.href.toString());

class BillInfor {

    constructor() {
        this.title = Selector("h1").withAttribute("class", "no-margin");
        this.myChosen = Selector("div").withText("Your Cerebral Membership").nextSibling("div").nth(4).find("div").nth(1);/* Selector("div").withAttribute("class","PaymentInfoInsurance__Name-sc-m4l5ru-4 ikhEXD"); */
        this.firstPrice = Selector("span").withText("for your first month").prevSibling("span");
        this.priceLater = Selector("span").withText("for your first month").nextSibling("span");
        this.promoCode = Selector("input").withAttribute("value", "CARE30");
        // form credit card infor
        this.nameOnCard = Selector("input").withAttribute("id", "fullName");
        this.cardNumber = Selector("input").withAttribute("name", "cardnumber");
        //Selector("input").withAttribute("id", "fullName").nextSibling("input").nth(0);
        //Selector("div").withAttribute("id","root").find("input").nth(2);
        //Selector("input").withAttribute("placeholder","Credit Card Number");
        this.outOfDate = Selector("input").withAttribute("name", "exp-date");
        this.cvc = Selector("input").withAttribute("name", "cvc");
        this.zip = Selector("input").withAttribute("placeholder", "ZIP");
        this.btnSubmit = Selector("button").withText("Submit");
        this.frame1 = Selector("iframe").nth(0);
        this.frame2 = Selector("iframe").nth(1);
        this.frame3 = Selector("iframe").nth(2);

    }

    async billingScreen(string) {
        let text = await this.title.with({ boundTestRun: testController }).innerText;
        console.log(getLocation());
        await testController.expect(text).eql(string);//Billing Information
    }

    async checkMyChoose(string) {
        let text = await this.myChosen.with({ boundTestRun: testController }).innerText;
        await testController.expect(text).eql(string);//Medication & Care Counseling
    }

    async myDiscountPrice(noteFirstPrice) {
        await testController.expect(this.firstPrice.with({ boundTestRun: testController }).exists).ok();
        let numFirstPrice = await this.firstPrice.with({ boundTestRun: testController }).innerText;
        numFirstPrice = numFirstPrice.replace(/[^0-9\.]+/g, "");
        console.log(numFirstPrice);
        await testController.expect(numFirstPrice).eql(noteFirstPrice);
    }

    async myPriceLater(notePriceLater) {
        await testController.expect(this.priceLater.with({ boundTestRun: testController }).exists).ok();
        let numPriceLater = await this.priceLater.with({ boundTestRun: testController }).innerText;
        numPriceLater = numPriceLater.replace(/[^0-9\.]+/g, "");
        console.log(numPriceLater);
        await testController.expect(numPriceLater).eql(notePriceLater);
    }

    async myPromoteCode(myCode) {
        let text = await this.promoCode.with({ boundTestRun: testController }).getAttribute("value");
        await testController.expect(text).eql(myCode);//CARE30
    }

    async inputCreditCardInfor(nameOnCard, cardNumber, outOfDate, cvc, zip) {
        await testController.scroll("bottom").wait(2000);
        await testController.typeText(this.nameOnCard, nameOnCard);//hoangthang

        await testController.switchToIframe(this.frame1);
        await testController.typeText(this.cardNumber, cardNumber)//4242424242424242
            .switchToMainWindow();

        await testController.switchToIframe(this.frame2);
        await testController.typeText(this.outOfDate, outOfDate)//1222
            .switchToMainWindow();

        await testController.switchToIframe(this.frame3);
        await testController.typeText(this.cvc, cvc)//111
            .switchToMainWindow();

        await testController.typeText(this.zip, zip).wait(3000);//77089
        await testController.click(this.btnSubmit).wait(11000);
    }

}

module.exports = new BillInfor();