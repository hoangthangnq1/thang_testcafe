const { Selector } = require('testcafe');


class ShippingInfor {

    constructor() {
        this.address1 = Selector("#input-address-line1");
        this.address2 = Selector("#input-address-line2");
        this.city = Selector("#input-city");
        this.state = Selector("#input-region");
        this.stateOption = Selector("#input-region").find("option");
        this.zipcode = Selector("input").withAttribute("name", "postal-code");
        this.btnContinue = Selector("button").withText("Continue");
        this.btnNext = Selector("button").withText("Next");
        
    }

    async inputShippingInfor(address1, address2, city, zipcode) {
        await testController.typeText(this.address1, address1);
        await testController.typeText(this.address2, address2);
        await testController.typeText(this.city, city);
        await testController.scroll("bottom");

        let dem = await this.stateOption.with({ boundTestRun: testController }).count;

        await testController.click(this.state).wait(1000);
        await testController.click(this.stateOption.nth(this.getRandomIntBetween(1, dem))).wait(3500);
        await testController.typeText(this.zipcode, zipcode).wait(2500);

        await testController.click(this.btnContinue).wait(3000);


    }

    async clickRandomState() {
        let dem = await this.stateOption.with({ boundTestRun: testController }).count;
        console.log(dem);
        var numOption = this.getRandomInt(dem);
        if (numOption == 0) {
            numOption = 1;
        }
        await testController.click(this.state).wait(2000);
        await testController.click(this.stateOption.nth(numOption)).wait(1000);
    }

    getRandomInt = (max) => {
        return Math.floor(Math.random() * max);
    }

    getRandomIntBetween(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min);
    }
}

module.exports = new ShippingInfor();