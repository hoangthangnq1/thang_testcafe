const { Selector } = require('testcafe');

class CheckLevel {

    constructor() {
        this.btnContinue = Selector("button").withText("Continue");
        this.btnNo = Selector("button").withText("No");
        this.level = Selector("span").withAttribute("data-testid", "indicator-result");

    }

    async checkMyLevelDisplayed() {
        await testController.expect(this.level.with({ boundTestRun: testController }).exists).ok();
    }

    async clickbtnContinue() {
        await testController.click(this.btnContinue).wait(1000);
    }

    async clickbtnNo() {
        await testController.click(this.btnNo).wait(2000);
    }



}

module.exports = new CheckLevel();