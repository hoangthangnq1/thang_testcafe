
const randomstring = require('randomstring');
const { Selector } = require('testcafe');
const Button = require('../../cores/Button');

class CheckoutQuestions {

    constructor() {
        this.labels = Selector("span").withAttribute("class", "checkbox-checkmark");
        this.btnConfirm = Selector("button").withText("Confirm");
        this.options = Selector("button").withAttribute("type", "button");
        this.weight = Selector("input").withAttribute("placeholder", "lbs");
        this.select1 = Selector("select").withAttribute("class", "custom-select input_height_weight").nth(0);
        this.select2 = Selector("select").withAttribute("class", "custom-select input_height_weight").nth(1);
        this.options1 = this.select1.find("option");
        this.options2 = this.select2.find("option");
        this.textareaDiagnosis = Selector("textarea").withAttribute("placeholder", "Please elaborate on your diagnosis.")
        this.textareaOption = Selector("textarea").withAttribute("placeholder", "Please elaborate on your answer.");
        this.textareaSubstances = Selector("textarea").withAttribute("placeholder", "Please elaborate on when you’ve used these substances and how often.");
        this.txtareaWhyTreatment = Selector("textarea");
        this.lastCheckBox = Selector("span").withText("None apply");
    }

    async clickRandomCheckBoxWithTextArea(Selector) {
        let count = await this.labels.with({ boundTestRun: testController }).count;
        let box = this.getRandomInt(count);
        await testController.click(this.labels.nth(box)).wait(2000);
        if (await Selector.with({ boundTestRun: testController }).exists) {
            await testController.typeText(Selector, randomstring.generate({ length: 10 })).wait(2000);
        }
    }

    async clickRandomOption() {
        let max = await this.options.with({ boundTestRun: testController }).count;
        if (await this.options.with({ boundTestRun: testController }).exists) {
            await testController.click(this.options.nth(this.getRandomInt(max)));
        }
    }

    async selectWeightandHeight(weight) {
        await testController.typeText(this.weight, weight).wait(1000);
        var count1 = await this.options1.with({ boundTestRun: testController }).count;
        await testController.click(this.select1).wait(1000);
        await testController.click(this.options1.nth(this.getRandomIntBetween(1, count1))).wait(1000);
        var count2 = await this.options1.with({ boundTestRun: testController }).count;
        await testController.click(this.select2).wait(1000);
        await testController.click(this.options2.nth(this.getRandomIntBetween(2, count2))).wait(1000);
    }

    async clickRandomCheckBox() {
        let max = await this.labels.with({ boundTestRun: testController }).count;
        if (await this.labels.with({ boundTestRun: testController }).exists) {
            await testController.click(this.labels.nth(max-1)).wait(1000);
        }
    }

    async clickRandomOptionLoop() {
        let count = 0;       
        for (let i = 0; i <= count; i++) {
            let max = await this.options.with({ boundTestRun: testController }).count;
            await testController.wait(1000);
            if (await this.options.with({ boundTestRun: testController }).exists) {
                count = count + 1;
                await testController.click(this.options.nth(this.getRandomInt(max)));
            } else {
                break;
            }
        }
    }

    async clickCheckBox() {
        await testController.click(this.lastCheckBox).wait(1000);
    }

    async inputTextArea() {
        await testController.typeText(this.txtareaWhyTreatment, randomstring.generate({ length: 10 })).wait(1000);
    }

    getRandomInt = (max) => {
        return Math.floor(Math.random() * max);
    }

    getRandomIntBetween(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min);
    }
}

module.exports = new CheckoutQuestions();

