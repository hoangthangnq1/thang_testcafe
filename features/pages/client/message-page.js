const { Selector } = require('testcafe');
const { ClientFunction } = require ('testcafe');



class MessagePage {

    constructor() {
        this.createAccount = Selector("input").withAttribute("value", "I don't have an account.");
        this.messageWelcome = Selector("span").withAttribute("class", "received_message");
    }

    async verifyPageURL(url) {
        const getURL = ClientFunction(() => window.location.href).with({ boundTestRun: testController });
        console.log("Main URL: "+await getURL());
        await testController.expect(await getURL()).contains(url);
    }

    async verifyMessageDisplay(string){
        await testController.expect(this.messageWelcome.with({ boundTestRun: testController }).exists).ok();
        await testController.expect(this.messageWelcome.with({ boundTestRun: testController }).innerText).contains(string);
    }
}

module.exports = new MessagePage();