const { Selector } = require('testcafe');

class PharmacyPage {

    constructor() {
        this.btnPickUp =Selector("button").withAttribute("type","pickup").find("span").nth(0);
        this.btnDelivery =Selector("button").withAttribute("type","delivery").find("span").nth(0);
        this.labels = Selector("label").withAttribute("class","radio-button-container");
        this.btnAlert = Selector("#button").nth(0);
    }

    async clickButtonPickUp() {
        console.log(await this.btnPickUp.with({ boundTestRun: testController }).innerText); 
        await testController.click(this.btnPickUp).wait(1000);  
    }

    async clickButtonDelivery() {
        await testController.click(this.btnDelivery).wait(1000);
    }

    async clickRandomPharmacy(){
        let max = await this.labels.with({ boundTestRun: testController }).count;
        await testController.click(this.labels.nth(this.getRandomInt(max))).wait(1000);
    }

   
    getRandomInt = (max) => {
        return Math.floor(Math.random() * max);
    }
}

module.exports = new PharmacyPage();