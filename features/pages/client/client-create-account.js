const { Selector } = require('testcafe');
var randomstring = require("randomstring");


class CreateAccount {

    constructor() {
        this.myEmail;
        this.myPassword;
        // account information page
        this.email = Selector("#input-email");
        this.password = Selector("#input-password");
        this.phoneNumber = Selector("input").withAttribute("placeholder", "Phone Number");
        this.confirm = Selector("input").withAttribute("name", "password_confirm");
        this.checkbox = Selector("label").withAttribute("for", "consent_checkbox");
        this.btnGetStarted = Selector("button").withText("Get started");
        // personal information page
        this.firstname = Selector("input").withAttribute("placeholder", "Legal First Name");
        this.lastname = Selector("input").withAttribute("name", "family-name");
        this.zipcode = Selector("input").withAttribute("name", "postal-code");
        this.date = Selector("input").withAttribute("name", "dob");
        this.btnContinue = Selector("button").withText("Continue");
    }

    setEmail = (email) => {
        this.myEmail = email;
    }

    setPassword = (password) => {
        this.myPassword = password;
    }

    getEmail = () => {
        return this.myEmail;
    }

    getPassword = () => {
        return this.myPassword;
    }


    async inputInforAccount(eMail, phoneNumber, passWord) {
        this.setEmail(eMail);
        this.setPassword(passWord);
        await testController.typeText(this.email, eMail);
        await testController.typeText(this.phoneNumber, phoneNumber);
        await testController.typeText(this.password, passWord);
        await testController.typeText(this.confirm, passWord);
        await testController.click(this.checkbox);
        await testController.click(this.btnGetStarted).wait(2000); 
    }

    async inputInforPersonal(firstname, lastname, zipcode, date) {
        await testController.typeText(this.firstname, firstname);
        await testController.typeText(this.lastname, lastname);
        await testController.typeText(this.zipcode, zipcode).wait(2000);
        await testController.typeText(this.date, date).wait(2000);
        await testController.click(this.btnContinue).wait(1000);
    }
}

module.exports = new CreateAccount();