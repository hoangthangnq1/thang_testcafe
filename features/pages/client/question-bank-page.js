const { Selector } = require('testcafe');

class ServicePage {

    constructor() {
        this.btnConfirm = Selector("button").withText("Confirm");
        this.btnNext = Selector("button").withText("Next");
        this.options = Selector("button").withAttribute("type", "button");
        this.labels = Selector("span").withAttribute("class", "checkbox-checkmark");
    }
    // choose service lines
    async clickServices(serviceName) {
        let serviceLine = Selector("label").withText(serviceName);
        await testController.click(serviceLine).wait(1000);
    }
    //actions
    async clickRandomOptionLoop() {
        let count = 0;       
        for (let i = 0; i <= count; i++) {         
            if (await this.options.with({ boundTestRun: testController }).exists) {
                let max = await this.options.with({ boundTestRun: testController }).count;
                count = count + 1;
                await testController.click(this.options.nth(this.getRandomInt(max)));
            } else {
                break;
            }
        }
    }
    async clickRandomCheckBox() {
        let max = await this.labels.with({ boundTestRun: testController }).count;
        await testController.click(this.labels.nth(this.getRandomInt(max))).wait(1000);
    }

    async clickConfirm() {
        await testController.click(this.btnConfirm).wait(6000);
    }

    async clickNext() {
        for (let i = 1; i <= 3; i++) {
            await testController.click(this.btnNext);
        }
    }

    getRandomInt = (max) => {
        return Math.floor(Math.random() * max);
    }

}

module.exports = new ServicePage();