const { Selector } = require('testcafe');

class PlanPage {


    constructor() {
        //this.btnViewMore = Selector("div").withAttribute("class","styled__ViewMorePlans-sc-1u7ffq8-1 kjQOQt");
        //this.btnViewMore =  Selector('section').nextSibling((node, idx, originNode)
        this.div = Selector('div').withText('Choose your plan.');
        this.btnViewMore = Selector('div').withText('Choose your plan.').nextSibling('div').nth(3)
        this.firstPlan = Selector('div').withText('Choose your plan.').nextSibling('div').nth(2).find("h3");
        this.btnStartToday = Selector("button").withText("Start Today");
        this.titlePlan = Selector("h3");
        this.discountPrice = this.btnStartToday.prevSibling("div").nth(0).child("div").nth(1);
        this.priceLater = this.btnStartToday.prevSibling("div").nth(0).child("div").nth(0);


        this.myDiscountPrice;
        this.myPriceLater;
    }

    async checkFirstPlanDisplayed(string) {
        await testController.wait(1000);
        await testController.expect(this.firstPlan.with({ boundTestRun: testController }).exists).ok();
        let myPlan = await this.firstPlan.with({ boundTestRun: testController }).innerText;
        await testController.expect(myPlan).eql(string);//"Medication &\nCare Counseling"
    }

    async allPlanDisplayed(plan1,plan2,plan3){
        let p1 = await this.titlePlan.nth(0).with({ boundTestRun: testController }).innerText;
        await testController.expect(p1).eql(plan1);
        let p2 = await this.titlePlan.nth(1).with({ boundTestRun: testController }).innerText;
        await testController.expect(p2).eql(plan2);
        let p3 = await this.titlePlan.nth(2).with({ boundTestRun: testController }).innerText;
        await testController.expect(p3).eql(plan3);
    }


    async clickViewMore() {
        await testController.scroll("bottom");
        await testController.click(this.btnViewMore).wait(2000);
    }

    async noteDiscountPrice() {
         let discountPrice = await this.discountPrice.with({ boundTestRun: testController }).innerText;
         discountPrice = discountPrice.replace(/[^0-9\.]+/g, "");
         this.myDiscountPrice = discountPrice;
         
    }

    async notePriceLater() {
        let priceLater = await this.priceLater.with({ boundTestRun: testController }).innerText;
        priceLater = priceLater.replace(/[^0-9\.]+/g, "");
        this.myPriceLater = priceLater;
    }

    async clickButtonStartToday() {
        await testController.click(this.btnStartToday).wait(2000);
    }





}

module.exports = new PlanPage();