const { Selector } = require('testcafe');
const CreateAccount = require("./client-create-account");

class ClientPage {

    constructor() {
        this.createAccount = Selector("input").withAttribute("value", "I don't have an account.");
        this.email = Selector("#input-email");
        this.password = Selector("#input-password");
        this.btnLogin = Selector("button").withText("Log in to My Account");
    }

    async navigateToPage(url) {
        await testController.navigateTo(url).wait(3000);
    }

    async navigateTo(url){
        await testController.setNativeDialogHandler(() => true);
        await testController.navigateTo(url).wait(2000);
    }


    async clickCreateAccount() {
        await testController.click(this.createAccount).wait(1000);
    }

    async loginMyAccount(myEmail,myPassword){
        console.log(myEmail);
        console.log(myPassword);
        await testController.typeText(this.email, myEmail);
        await testController.typeText(this.password, myPassword);
        await testController.click(this.btnLogin).wait(3000);
    }
}

module.exports = new ClientPage();