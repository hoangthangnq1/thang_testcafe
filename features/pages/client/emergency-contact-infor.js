const { Selector } = require('testcafe');
const randomstring = require("randomstring");

class Delivery {

    constructor() {
        this.firstname = Selector("input").withAttribute("name", "given-name");
        this.lastname = Selector("input").withAttribute("name", "family-name");
        this.phoneNumber = Selector("input").withAttribute("placeholder", "Phone Number");
        this.btnContinue = Selector("button").withText("Continue");
        this.labels = Selector("span").withAttribute("class", "checkbox-checkmark");
        this.textarea = Selector("textarea").withAttribute("placeholder", "Please elaborate on your diagnosis.")
        this.btnConfirm = Selector("button").withText("Confirm");
    }

    async inputEmergencyContact(firstname, lastname, phoneNumber,) {
        await testController.typeText(this.firstname, firstname);
        await testController.typeText(this.lastname, lastname);
        await testController.typeText(this.phoneNumber, phoneNumber);
        await testController.click(this.btnContinue).wait(2000);
    }

    async clickRandomCheckBoxWithTextArea() {
        let max = await this.labels.with({ boundTestRun: testController }).count;
        let numCheckBox = this.getRandomInt(max);
        if (numCheckBox != count) {
            await testController.click(this.labels.nth(numCheckBox)).wait(2000);
        } else {
            await testController.click(this.labels.nth(numCheckBox)).wait(2000);
            await testController.typeText(this.textarea, randomstring);
        }

        await testController.click(this.btnConfirm).wait(2000);
    }
}

module.exports = new Delivery();