const { Selector } = require('testcafe');

class UpgradePage {

    constructor() {
        this.nofAddTherapy = Selector("button").withText("Continue with current plan").prevSibling(0).child("div").nth(1).child("h3")//Selector("h3").withText("Add Therapy to improve your care");
        this.btnContinuePlan = Selector("button").withText("Continue with current plan");
        this.options = Selector("button").withAttribute("type", "button");
    }

    async upgradePlan(string) {
        let text = await this.nofAddTherapy.with({ boundTestRun: testController }).innerText;
        await testController.expect(text).eql(string);//"Add Therapy to improve \nyour care" 

    }

    async clickContinueCurrentPlan() {
        let count = 0;
        for (let i = 0; i <= count; i++) {
            if (await this.btnContinuePlan.with({ boundTestRun: testController }).exists) {
                await testController.scroll("bottom");
                count = count + 1;
                await testController.click(this.btnContinuePlan).wait(2000);
            } else {
                console.log(count);
                break;
            }
        }
    }

    async clickRandomOption() {
        let max = await this.options.with({ boundTestRun: testController }).count;
        if (await this.options.with({ boundTestRun: testController }).exists) {
            await testController.click(this.options.nth(this.getRandomInt(max)));
        }
    }

    getRandomInt = (max) => {
        return Math.floor(Math.random() * max);
    }
}

module.exports = new UpgradePage();