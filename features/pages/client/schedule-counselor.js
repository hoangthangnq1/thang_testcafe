const { Selector } = require('testcafe');

class ScheduleCounSelor {

  constructor() {
    this.days = Selector("abbr");
    this.dayOption = Selector('div.react-calendar__month-view__days').child('button:enabled');
    this.dayArray = [];

    this.note = Selector("p").withText("If you don't see a suitable time, contact support@getcerebral.com");
    this.calendarTime = this.note.prevSibling("ul").find("li").child("button");

    this.btnConfirm = Selector("button").withText("Confirm");
    this.textTitle = Selector("div").withText("Please schedule an appointment with a cerebral prescriber.");
    this.btnNextPrescriber = this.textTitle.nextSibling("button").nth(1);
    this.rdbOption = Selector("span").withAttribute("class", "radio-button");
    this.cbxOption = Selector("label");
    this.btnNext = Selector("button").child("span").withText("Next");
    this.messageWarning = this.btnConfirm.nextSibling("p");
  }

  async clickDay() {
    await testController.expect(this.dayOption.with({ boundTestRun: testController }).exists).ok();
    let numDay = await this.dayOption.with({ boundTestRun: testController }).count
    console.log(numDay);
    for (let i = 0; i <= numDay - 1; i++) {
        this.dayArray.push(i);
    }
    await testController.click(this.dayOption.nth(this.getRandomIntNotDuplicate(this.dayArray))).wait(2000);
}

async clickTime() {
    let timeArray = [];
    await testController.expect(this.calendarTime.with({ boundTestRun: testController }).exists).ok();
    let numCalendar = await this.calendarTime.with({ boundTestRun: testController }).count;
    for (let i = 1; i <= numCalendar - 1; i++) {
        timeArray.push(i);
    }
    await testController.click(this.calendarTime.nth(0)).wait(2000);
    await testController.click(this.btnConfirm).wait(1000);
    while (await this.messageWarning.with({ boundTestRun: testController }).exists) {
        if (timeArray.length === 0) {
            await this.clickDay();
            await testController.expect(this.calendarTime.with({ boundTestRun: testController }).exists).ok();
            let numCalendar = await this.calendarTime.with({ boundTestRun: testController }).count;
            for (let i = 0; i <= numCalendar - 1; i++) {
                timeArray.push(i);
            }
        }
        await testController.click(this.calendarTime.nth(this.getRandomIntNotDuplicate(timeArray))).wait(1000);
        await testController.click(this.btnConfirm).wait(2000);        
    }
}



  getRandomIntBetween(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
  }

  createArrayOfNumber(start, end, myArray) {
    for (let i = start; i <= end; i++) {
      myArray.push(i);
    }
    return myArray;
  }

  getRandomIntNotDuplicate(array) {
    if (array.length === 0) {
      console.log('No more random number');
      return;
    }
    let randomIndex = this.getRandomIntBetween(0, array.length - 1);
    let randomNumber = array[randomIndex];
    array.splice(randomIndex, 1);
    return randomNumber;
  }

}

module.exports = new ScheduleCounSelor();