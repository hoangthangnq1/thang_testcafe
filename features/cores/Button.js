const { Selector } = require('testcafe');

class Button {

    constructor() {
        this.button =Selector("button");
    }

    async click(mySelector){
        await testController.click(mySelector).wait(1000);
    }

    async clickButton(string , number) {
        for (let i = 1; i <= number; i++) {
               await testController.click(this.button.withText(string)).wait(1000);    
        }    
    }

    async clickButtonWithTime(string , number , time) {
        for (let i = 1; i <= number; i++) {
               await testController.click(this.button.withText(string)).wait(time);    
        }    
    }

    async clickRandomOption() {
        let count = 0;       
        for (let i = 0; i <= count; i++) {
            let max = await this.options.with({ boundTestRun: testController }).count;
            await testController.wait(1000);
            if (await this.options.with({ boundTestRun: testController }).exists) {
                count = count + 1;
                await testController.click(this.options.nth(this.getRandomInt(max)));
            } else {
                break;
            }
        }
    }

    getRandomInt = (max) => {
        return Math.floor(Math.random() * max);
    }

    getRandomIntBetween(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min);
    }
   
    createArrayOfNumber(start, end, myArray) {
        for (let i = start; i <= end; i++) {
            myArray.push(i);
        }
        return myArray;
    }

    getRandomIntNotDuplicate(array) {
        if (array.length === 0) {
            console.log('No more random number');
            return;
        }
        let randomIndex = this.getRandomIntBetween(0, array.length - 1);
        let randomNumber = array[randomIndex];
        array.splice(randomIndex, 1);
        return randomNumber;
    }
}

module.exports = new Button();