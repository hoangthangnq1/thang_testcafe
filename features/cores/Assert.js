const { Selector } = require('testcafe');

class Assert{

    async AssertEqual(mySelector,string) {
        await testController.expect(mySelector.with({ boundTestRun: testController }).exists).ok();
        let text = await mySelector.with({ boundTestRun: testController }).innerText;
        await testController.expect(text).eql(string);
    }

    async AssertContains(mySelector,string) {
        await testController.expect(mySelector.with({ boundTestRun: testController }).exists).ok();
        let text = await mySelector.with({ boundTestRun: testController }).innerText;
        await testController.expect(text).contains(string);
    }

    async AssertTimeout(mySelector,string,time){
        await testController.expect(mySelector.with({ boundTestRun: testController }).exists).ok();
        let text = await mySelector.with({ boundTestRun: testController }).innerText;
        await testController.expect(text).eql(string,"check element text", { timeout: time });
    }

}

module.exports = new Assert();