Feature: test celebral website
        # The ADHD service needs to be active in the clients state
        # As I am a client who is seeking for an online ADHD cessation service that is available in my state
        # AND I found a link that directs me to Cerebral website
        # @NCI-842   
         Scenario:Create an account and check the function    
        Given I navigate to login page from the landing page
        When I click on “I dont have an account.”
        And complete account information step
        And completed personal information step
        And select "ADHD" service line
        And answer a list of questions regarding to ADHD
        Then I see Preliminary Screen Indicators screen showing my “ADHD“ level
        When I click on “Continue”
        And I click on “Start Today” on Medication & Care Counseling plan
        Then I get navigated to Billing Information screen
        And I see "Medication & Care Counseling"
        And the price is for first month is $"7"
        And the price for month later is $"20"
        When I complete check out
        And click “Continue with current plan”
        And complete shipping information
        And complete prescription delivery method
        And complete ADHD post checkout questions
        And complete “Identity Verification“
        And continue to the client portal
        Then I get navigated to Message box chat
        And see greeting message from Cerebral Care team