Feature: test celebral website
        Check website functionality from create acount
        @TC_01
        Scenario:Create an account and check the function
        Given I navigate to login page from the landing page
        When I click on “I dont have an account.“
        And complete account information step
        And complete personal information step
        And select "Anxiety" service line
        And I select “Addiction” - "Alcohol" service lines
        And answer a list of questions regarding to Anxiety and Alcohol
        Then I see Preliminary Screen Indicators screen showing my Anxiety level and Alcohol level
        When I click Continue
        Then I see Medication Care Counseling plan
        When I click VIEW MORE PLANS
        Then I see “Medication & Care Counseling“, “Medication & Therapy“, “Therapy“ plans
        When I note down the first month price and discount price of “Medication & Care Counseling“ plan
        And select “Medication & Care Counseling“ plan
        And get navigated to Billing Information screen
        Then I see “Medication & Care Counseling“ which I chose
        And the price for first month is like I noted and the price for month later is $20
        And the promo code is “CARE30“
        When I complete check out
        Then I see “Add Therapy to improve your care“ plan
        When I click “Continue with current plan“ to skip
        And I complete shipping information
        And I complete delivery method
        And complete post checkout questions
        And complete “Identity Verification“
        And select “I prefer a video visit“
        And schedule appointment with prescriber
        And schedule appointment with counselor
        And click “Continue to the client portal“
        Then I get navigated to Message box chat
        And see Welcome Message from care counselor

        


